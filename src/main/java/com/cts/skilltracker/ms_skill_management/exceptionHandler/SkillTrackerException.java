package com.cts.skilltracker.ms_skill_management.exceptionHandler;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SkillTrackerException extends RuntimeException{
    String code;
    public SkillTrackerException(String code, String message){
        super(message);
        this.code = code;
    }
    public SkillTrackerException(String message){
        super(message);
    }

}
