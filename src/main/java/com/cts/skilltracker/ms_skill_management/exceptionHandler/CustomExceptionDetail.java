package com.cts.skilltracker.ms_skill_management.exceptionHandler;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomExceptionDetail {
    private String code;
    private String message;
}
