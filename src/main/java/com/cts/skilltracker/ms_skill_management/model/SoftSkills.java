package com.cts.skilltracker.ms_skill_management.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.OffsetDateTime;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection="softSkills")
public class SoftSkills extends Auditable<SoftSkills> {

    @Id
    private String id;

    @Indexed(unique = true)
    private String associateId;

    @Min(value = 0 , message = "Expertise Level can be between 0-20")
    @Max(value = 20 , message = "Expertise Level can be between 0-20")
    private int spoken;

    @Min(value = 0 , message = "Expertise Level can be between 0-20")
    @Max(value = 20 , message = "Expertise Level can be between 0-20")
    private int communication;

    @Min(value = 0 , message = "Expertise Level can be between 0-20")
    @Max(value = 20 , message = "Expertise Level can be between 0-20")
    private int aptitude;

}
