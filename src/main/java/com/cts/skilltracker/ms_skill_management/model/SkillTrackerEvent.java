package com.cts.skilltracker.ms_skill_management.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SkillTrackerEvent {

    @NotNull(message="Please enter associate id")
    private String associateId;

    @Valid
    private TechSkills techSkills;

    @Valid
    private SoftSkills softSkills;
}
