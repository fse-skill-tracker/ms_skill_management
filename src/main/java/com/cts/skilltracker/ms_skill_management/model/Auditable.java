package com.cts.skilltracker.ms_skill_management.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.util.Date;

@Data
public abstract class Auditable<U> {

    @CreatedDate
    private Date createDate;

    @LastModifiedDate
    private Date lastModifiedDate;
}

