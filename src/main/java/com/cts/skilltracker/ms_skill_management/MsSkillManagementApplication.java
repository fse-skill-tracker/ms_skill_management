package com.cts.skilltracker.ms_skill_management;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
@EnableKafka
@EnableMongoRepositories
@EnableMongoAuditing
public class MsSkillManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsSkillManagementApplication.class, args);
	}

}
