package com.cts.skilltracker.ms_skill_management.controller;

import com.cts.skilltracker.ms_skill_management.model.SkillTrackerEvent;
import com.cts.skilltracker.ms_skill_management.model.TechSkills;
import com.cts.skilltracker.ms_skill_management.service.SkillManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/skill-tracker/skills")
@Validated
public class UserSkillController {

    @Autowired
    private SkillManagementService skillService;

    @GetMapping("/search-skill")
    public String searchSkill(String associateId){
        return "";
    }

    @PutMapping("/update-profile/{associateId}")
    public ResponseEntity updateSkill(@Valid   @RequestBody SkillTrackerEvent userSkills, @PathVariable String associateId){
        skillService.updateSkill(associateId, userSkills);
        return ResponseEntity.status(HttpStatus.OK).body("Skill updated successfully");
    }

    @GetMapping("/search-skill/skill/{criteria}")
    public ResponseEntity<List<SkillTrackerEvent>> searchUserBySkill(@PathVariable String criteria){
        HttpStatus status = HttpStatus.OK;
        List<SkillTrackerEvent> skills = skillService.searchUserBySkills(criteria, 10);
        if(skills == null || skills.size() == 0) {
            status = HttpStatus.NOT_FOUND;
        }
        return ResponseEntity.status(status).body(skills);
    }

    @GetMapping("/search-skill/byAssociates")
    public ResponseEntity<List<SkillTrackerEvent>> searchUserByAssociates(@RequestParam List<String> associateList){
        HttpStatus status = HttpStatus.OK;
        List<SkillTrackerEvent> skills = skillService.searchUsersByAssociateList(associateList);
        if(skills == null || skills.size() == 0){
            status = HttpStatus.NOT_FOUND;
        }
        return ResponseEntity.status(status).body(skills);
    }

    @GetMapping("/health-check")
    public String healthCheck(){
        return "true";
    }


}
