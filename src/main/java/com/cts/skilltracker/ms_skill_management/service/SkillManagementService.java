package com.cts.skilltracker.ms_skill_management.service;

import com.cts.skilltracker.ms_skill_management.exceptionHandler.SkillTrackerException;
import com.cts.skilltracker.ms_skill_management.model.SkillTrackerEvent;
import com.cts.skilltracker.ms_skill_management.model.SoftSkills;
import com.cts.skilltracker.ms_skill_management.model.TechSkills;
import com.cts.skilltracker.ms_skill_management.repository.SoftSkillsRepository;
import com.cts.skilltracker.ms_skill_management.repository.TechSkillsRepository;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SkillManagementService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SkillManagementService.class);

    @Autowired
    private TechSkillsRepository techSkillRepo;

    @Autowired
    private SoftSkillsRepository softSkillRepo;

    @KafkaListener(topics="${spring.kafka.topic.name}", groupId="${spring.kafka.consumer.group-id}")
    public void consume(SkillTrackerEvent stEvent){

        System.out.println("consume");
        LOGGER.info(String.format("skill event %s", stEvent.toString()));
        TechSkills techSkills = stEvent.getTechSkills();
        techSkills.setAssociateId(stEvent.getAssociateId());
        addTechSkills(techSkills);

        SoftSkills softSkills = stEvent.getSoftSkills();
        softSkills.setAssociateId(stEvent.getAssociateId());

        addSoftSkills(softSkills);
    }

    private void addSoftSkills(SoftSkills softSkills) {
        softSkillRepo.insert(softSkills);
    }

    private void addTechSkills(TechSkills techSkills){
        techSkillRepo.insert(techSkills);
    }

    public void updateSkill(String associateId, SkillTrackerEvent stEvent){
        TechSkills techSkills = techSkillRepo.findByAssociateId(associateId);
        if(techSkills == null){
            throw new SkillTrackerException("ASSOCIATE_NOT_FOUND", "The given associate id is not present");
        }else{
            boolean isOldUpdate = checkLastUpdatedDate(techSkills.getCreateDate(), techSkills.getLastModifiedDate());
            if(!isOldUpdate){
                throw new SkillTrackerException("PROFILE_UPDATE_ERROR", "Profile Updated less than 10 days ago");
            }
            String _objectId = techSkills.getId();
            techSkills = createUpdatedTechSkillDocument(stEvent, techSkills);
            techSkillRepo.save(techSkills);
        }
        SoftSkills softSkills = softSkillRepo.findByAssociateId(associateId);
        if(softSkills == null){
            throw new SkillTrackerException("ASSOCIATE_NOT_FOUND", "The given associate id is not present");
        }else{
            String _ssObjectId = softSkills.getId();
            softSkills=  createUpdatedSoftSkillDocument(stEvent,softSkills);
            softSkillRepo.save(softSkills);
        }
    }

    private boolean checkLastUpdatedDate(Date createDate, Date lastModifiedDate) {
        boolean updateFlag = true;
        ZonedDateTime now = ZonedDateTime.now();
        ZonedDateTime tenDaysAgo = now.plusDays(-10);
        if(lastModifiedDate != null){
            if (!(lastModifiedDate.toInstant().isBefore(tenDaysAgo.toInstant()))) {
                updateFlag = false;
            }

        }else if(createDate != null){
            if (!(createDate.toInstant().isBefore(tenDaysAgo.toInstant()))) {
                updateFlag = false;
            }

        }
        return  updateFlag;
    }


    private TechSkills createUpdatedTechSkillDocument(SkillTrackerEvent skillTrackerEvent, TechSkills techSkills){
        if(skillTrackerEvent.getTechSkills() != null){
            TechSkills ts = skillTrackerEvent.getTechSkills();
            if(ts.getHtmlJS() > 0) {
                techSkills.setHtmlJS(ts.getHtmlJS());
            }
            if(ts.getHibernate() > 0) {
                techSkills.setHibernate(ts.getHibernate());
            }
            if(ts.getAngular() > 0) {
                techSkills.setAngular(ts.getAngular());
            }
            if(ts.getAws() > 0) {
                techSkills.setAws(ts.getAws());
            }
            if(ts.getSpring() > 0) {
                techSkills.setSpring(ts.getSpring());
            }
            if(ts.getDocker() > 0) {
                techSkills.setDocker(ts.getDocker());
            }
            if(ts.getGit() > 0) {
                techSkills.setGit(ts.getGit());
            }
            if(ts.getReact() > 0) {
                techSkills.setReact(ts.getReact());
            }
            if(ts.getRestful() > 0) {
                techSkills.setRestful(ts.getRestful());
            }
            if(ts.getJenkins() > 0) {
                techSkills.setJenkins(ts.getJenkins());
            }
        }
        return techSkills;
    }

    private SoftSkills createUpdatedSoftSkillDocument(SkillTrackerEvent skillTrackerEvent, SoftSkills softSkills){
        if(skillTrackerEvent.getSoftSkills() != null){
            SoftSkills ss = skillTrackerEvent.getSoftSkills();
            if(ss.getAptitude() > 0){
                softSkills.setAptitude(ss.getAptitude());
            }
            if(ss.getCommunication() > 0){
                softSkills.setCommunication(ss.getCommunication());
            }
            if(ss.getSpoken() > 0){
                softSkills.setSpoken(ss.getSpoken());
            }
        }
        return softSkills;
    }



    public List<SkillTrackerEvent> searchUserBySkills(String criteria, int i) {
        List<SkillTrackerEvent> fullSkillList = null;
        List<TechSkills> techSkills  = techSkillRepo.findBySkill(criteria, i);
        if(techSkills !=null && techSkills.size() > 0){
            List<String> associateList = techSkills.stream().map(x->x.getAssociateId()).collect(Collectors.toList());
            List<SoftSkills> softSkills = softSkillRepo.findByAssociateIdIn(associateList);
            fullSkillList = createSkillList(associateList, techSkills, softSkills);
        }
        return fullSkillList;
    }

    public List<SkillTrackerEvent> searchUsersByAssociateList(List<String> associateList){
        List<TechSkills> techSkills  = techSkillRepo.findByAssociateIdIn(associateList);
        List<SoftSkills> softSkills = softSkillRepo.findByAssociateIdIn(associateList);

        List<SkillTrackerEvent> fullSkillList = createSkillList(associateList, techSkills, softSkills);
        return fullSkillList;
    }

    public List<SkillTrackerEvent> createSkillList(List<String> associateList, List<TechSkills> techSkills, List<SoftSkills> softSkills){
        List<SkillTrackerEvent> fullSkillList = new ArrayList<>();
        associateList.stream().forEach(x->{
            SkillTrackerEvent skEvent = new SkillTrackerEvent();
            Optional<TechSkills> ts = techSkills.stream().filter(y-> StringUtils.equalsIgnoreCase(y.getAssociateId(),x)).findFirst();
            if(ts.isPresent()){
                skEvent.setAssociateId(x);
                skEvent.setTechSkills(ts.get());
            }
            Optional<SoftSkills> ss = softSkills.stream().filter(y-> StringUtils.equalsIgnoreCase(y.getAssociateId(),x)).findFirst();
            if(ss.isPresent()){
                skEvent.setSoftSkills(ss.get());
            }
            fullSkillList.add(skEvent);
        });
        return  fullSkillList;
    }
 }
