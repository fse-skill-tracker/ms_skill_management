package com.cts.skilltracker.ms_skill_management.repository;

import com.cts.skilltracker.ms_skill_management.model.SoftSkills;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface SoftSkillsRepository extends MongoRepository<SoftSkills, String> {
    public SoftSkills findByAssociateId(String associateId);

    public List<SoftSkills> findByAssociateIdIn(List<String> associateId);
}
