package com.cts.skilltracker.ms_skill_management.repository;

import com.cts.skilltracker.ms_skill_management.model.TechSkills;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface TechSkillsRepository extends MongoRepository<TechSkills, String> {
    public TechSkills findByAssociateId(String associateId);

    @Query("{?0 : {$gt: ?1}}")
    public List<TechSkills> findBySkill(String skill, int value);

    public List<TechSkills> findByAssociateIdIn(List<String> associateId);


}
